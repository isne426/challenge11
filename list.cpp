#include <iostream>
#include "list.h"

List::~List() {
	for(Node *p; !isEmpty(); ){
		p=head->next;
		delete head;
		head=p;
	}
}																														//Starter.

void List::headPush(int value){

	Node *create = new Node(value, head, NULL);
	head = create;
	
	if(tail == 0){
		tail = head;
	}
}																														//Add an element to the front of the list.


void List::tailPush(int value){
	
	Node *create = new Node(value, NULL, tail);
	
	if(tail != 0){
		
		tail->next = create;
	}
	tail = create;
}																														//Add an element to the end of the list.

int List::headPop(){
	
	if(head == 0){
		
		return 0;
	}
	int value = head->info;
	
	if(head == tail){
		
		delete tail;
		tail = 0;
		head = 0;
	}else{
		
		Node *del;
		del = head;
		head = del->next;
		head->prev = NULL;
		delete del;
	}
	return value;
}																														//Remove an element of the front of the list, return a value that was stored in that node.

int List::tailPop(){
	
	if(tail == 0){
		
		return 0;
	}
	
	int value = tail->info;
	if(tail == head){
		
		delete tail;
		tail = 0;
		head = 0;	
	}else{
		
		Node *del = tail->prev;
		delete tail;
		tail = del;
		tail->next = NULL;
	}
	return value;
}																														//Remove an element of the end of the list, return a value that was stored in that node.

void List::deleteNode(int value){

	Node *del = head, *temporary = 0, *temporary2 = 0;
	
	if(head->info == value){
		headPop();
	}else if(tail->info == value){
		tailPop();
	}else{
		
		del = del->next;
		temporary = head;
		temporary2 = del->next;
		
		while(del != tail){
			if(del->info == value){
				
				temporary->next = temporary2;
				temporary2->prev = temporary;
				delete del;
				break;
			}
			
			temporary = del;
			
			del = temporary2;
			
			temporary2 = temporary2->next;
		}
	}
}																													//Remove an element that contains the exact value.

bool List::isInList(int value){
	
	Node *index = head;
	while(index != NULL){
		
		if(index->info == value){
			return true;
		}
		index = index->next;
	}
	return false;
}																													//Finding and returning what's in the list.

void List::showAll(){
	
	Node *index = head;
	while(index != NULL){
		std::cout << index->info << " ";
		index = index->next;
	}																												//Displaying all elements in the list.
}
