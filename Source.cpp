#include <iostream>
#include "list.h"

using namespace std;

int main(){																											//Starter.
	List List;																										//Demonstrate.
	int tmp;
	cout << "Start: is the list empty? : ";
	
	
	if(List.isEmpty()){
		
		cout << "Yes" << endl;	
	}else{
		
		cout << "No" << endl;	
	}
	
	
	cout << "Add 3, 4, 5 to the head of the list." << endl;
	List.headPush(3);
	List.headPush(4);
	List.headPush(5);
	
	
	cout << "Current list : ";
	List.showAll();
	
	cout << endl << endl << "Add 2, 1, 0 to the end of the list." << endl;
	List.tailPush(2);
	List.tailPush(1);
	List.tailPush(0);
	
	
	cout << "Current list : ";
	List.showAll();
	cout << endl << endl << "Remove elements from the head and the end of the list." << endl;
	cout << "Removed element : ";
	
	tmp = List.headPop();
	cout << tmp << " ";
	tmp = List.tailPop();
	cout << tmp << endl;
	cout << "Current list : ";
	
	
	List.showAll();
	cout << endl << endl << "Is there number 3 in the list? : ";
	
	
	if(List.isInList(3)){
		
		cout << "Yes" << endl;
		cout << "Remove 13 from the list." << endl;
		List.deleteNode(3);
	}else{
		
		cout << "No" << endl;	
	}
	
	cout << "Current list : ";
	List.showAll();
	cout << endl << endl << "- END -";
	
	
	return 0;
}
